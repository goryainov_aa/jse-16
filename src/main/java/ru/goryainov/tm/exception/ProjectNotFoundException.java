package ru.goryainov.tm.exception;

public class ProjectNotFoundException extends Exception {
    public ProjectNotFoundException(String errMsg){
        super(errMsg);
    }
}
