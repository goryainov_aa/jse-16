package ru.goryainov.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.repository.ProjectRepository;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class ProjectService extends AbstractService{

    private final Logger logger = LogManager.getLogger(ProjectService.class);

    public static final ProjectService Instance = new ProjectService();

    private ProjectService() {

    }

    public Project create(final String name, final String description, final User user) {
        logger.trace("create -> name = {}, description = {}, userId = {}", name, description, user.getId());
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (user == null) return null;
        return ProjectRepository.Instance.create(name, description, user);
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        logger.trace("update -> id = {}, name = {}, description = {}, userId = {}", id, name, description, userId);
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return ProjectRepository.Instance.update(id, description, userId);
    }

    public void clear(final Long userId) {
        logger.trace("update -> userId = {}", userId);
        ProjectRepository.Instance.clear(userId);
    }

    public Project findByName(final String name, final Long userId) {
        logger.trace("findByName -> name = {}, userId = {}", name, userId);
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return ProjectRepository.Instance.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException {
        logger.trace("findById -> id = {}, userId = {}", id, userId);
        if (id == null) return null;
        if (userId == null) return null;
        return ProjectRepository.Instance.findById(id, userId);
    }

    public Project findById(final Long id) throws ProjectNotFoundException {
        logger.trace("findById -> id = {}", id);
        if (id == null) return null;
        return ProjectRepository.Instance.findById(id);
    }

    public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException {
        logger.trace("removeById -> id = {}, userId = {}", id, userId);
        if (id == null) return null;
        if (userId == null) return null;
        return ProjectRepository.Instance.removeById(id, userId);
    }

    public Project removeByName(final String name, final Long userId) {
        logger.trace("removeByName -> name = {}, userId = {}", name, userId);
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return ProjectRepository.Instance.removeByName(name, userId);
    }
    public List<Project> findAll(final Long userId) {
        logger.trace("findAll -> userId = {}", userId);
        return ProjectRepository.Instance.findAll(userId);
    }


    public List<Project> findAll() {
        return ProjectRepository.Instance.findAll();
    }

    /**
     * Cоздания проекта
     */
    public int createProject(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Create project]");
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        User user_prj = UserService.Instance.getUserById(userId);
        ProjectService.Instance.create(name, description, user_prj);
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Изменение проекта по идентификатору
     */
    public int updateProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Update project]");
        System.out.println("[Please, enter project id:]");
        final long id = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Project project = ProjectService.Instance.findById(id, userId);
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanNextLine();
        ProjectService.Instance.update(project.getId(), name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по имени
     */
    public int removeProjectByName(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Remove project by name]");
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Project project = ProjectService.Instance.removeByName(name, userId);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по идентификатору
     */
    public int removeProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Remove project by id]");
        System.out.println("[Please, enter project id:]");
        final long id = scanNextLong();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Project project = ProjectService.Instance.removeById(id, userId);
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Вывод очистки проекта
     */
    public int clearProject(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Clear project]");
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        ProjectService.Instance.clear(userId);
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Просмотр проекта по идентификатору
     */
    public int viewProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        System.out.println("Enter, project id: ");
        final Long id = scanNextLong();
        User user = UserService.Instance.getUserBySession(sessionId);
        if (user == null) return 1;
        final Project project = ProjectService.Instance.findById(id, user.getId());
        if (checkAdminGrants(user) || user.getId().equals(project.getUserId())) {
            viewProject(project);
            return 0;
        } else System.out.println("You don't have enough privileges!");
        return 0;
    }


    /**
     * Просмотр списка проектов
     */
    private void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[View project]");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("UserId: " + project.getUserId());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка проектов
     */
    public int listProject(final UUID sessionId) {
        User user = UserService.Instance.getUserBySession(sessionId);
        if (user == null) return 1;
        List<Project> projectList = null;
        if (checkAdminGrants(user)) projectList = ProjectService.Instance.findAll();
        else projectList = ProjectService.Instance.findAll(user.getId());
        Collections.sort(projectList, Comparator.comparing(Project::getName));
        logger.info("[List project]");
        int index = 1;
        for (final Project project : projectList) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + " description " + project.getDescription() + " user: " + UserService.Instance.getUserById(project.getUserId()).getLogin());
            index++;
        }
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Назначение проекта пользователю
     */
    public int addProjectToUserId() throws ProjectNotFoundException {
        logger.info("[Add project to user]");
        System.out.println("Enter, project id: ");
        final Long projectId = scanNextLong();
        System.out.println("Enter, user id: ");
        final Long userId = scanNextLong();
        if (userId == null || projectId == null) return 1;
        Project project;
        project = ProjectService.Instance.findById(projectId);
        project.setUserId(userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * экспорт в json
     */
    public void exportToJSON() throws IOException {
        logger.info("[Export project data to JSON]");
        ProjectRepository.Instance.exportToJSON();
        System.out.println("[Ok]");
    }

    /**
     * импорт из json
     */
    public void importByJSON() throws IOException {
        logger.info("[Import project data by JSON]");
        ProjectRepository.Instance.importByJSON();
        System.out.println("[Ok]");
    }

    /**
     * экспорт в xml
     */
    public void exportToXML() throws IOException {
        logger.info("[Export project data to XML]");
        ProjectRepository.Instance.exportToXML();
        System.out.println("[Ok]");
    }

    /**
     * импорт из xml
     */
    public void importByXML() throws IOException {
        logger.info("[Import project data by XML]");
        ProjectRepository.Instance.importByXML();
        System.out.println("[Ok]");
    }

}
