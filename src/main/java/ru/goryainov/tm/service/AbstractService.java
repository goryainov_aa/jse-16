package ru.goryainov.tm.service;

import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.enumerated.Role;

import java.util.Scanner;

public class AbstractService {
    private final Scanner scanner = new Scanner(System.in);

    protected String scanNextLine() {
        return scanner.nextLine();
    }

    protected Long scanNextLong() {
        try {
            return scanner.nextLong();
        } finally {
            scanner.nextLine();
        }
    }

    protected Integer scanNextInt() {
        try {
            return scanner.nextInt();
        } finally {
            scanner.nextLine();
        }
    }

    public boolean checkAdminGrants(final User user) {
        if (user.getRole().toString().equals(Role.ADMIN.toString())) return true;
        return false;
    }
}
