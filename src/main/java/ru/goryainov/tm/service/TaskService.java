package ru.goryainov.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.repository.ProjectRepository;
import ru.goryainov.tm.repository.TaskRepository;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class TaskService extends AbstractService{

    private final Logger logger = LogManager.getLogger(TaskService.class);

    public static final TaskService Instance = new TaskService();

    private TaskService() {
        
    }

    public Task create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.create(name, userId);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.update(id, description, userId);
    }

    public void clear(final Long userId) {
        TaskRepository.Instance.clear(userId);
    }

    public Task findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.findByName(name, userId);
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.findById(id, userId);
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        if (id == null) return null;
        return TaskRepository.Instance.findById(id);
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.removeById(id, userId);
    }

    public Task removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return TaskRepository.Instance.removeByName(name, userId);
    }

    public List<Task> findAll(final Long userId) {
        if (userId == null) return null;
        return TaskRepository.Instance.findAll(userId);
    }

    public List<Task> findAll() {
        return TaskRepository.Instance.findAll();
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id, final Long userId) {
        if (projectId == null || id == null || userId == null) return null;
        return TaskRepository.Instance.findByProjectIdAndId(projectId, id, userId);
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        if (projectId == null || userId == null) return null;
        return TaskRepository.Instance.findAllByProjectId(projectId, userId);
    }

    /**
     * Вывод создания задачи
     */
    public int createTask(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Create task]");
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        TaskService.Instance.create(name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Изменение задачи по идентификатору
     */
    public int updateTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Update task]");
        System.out.println("[Please, enter task id:]");
        final long id = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Task task = TaskService.Instance.findById(id, userId);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanNextLine();
        TaskService.Instance.update(task.getId(), name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистка задачи
     */
    public int clearTask(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Clear task]");
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        TaskService.Instance.clear(userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по имени
     */
    public int removeTaskByName(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Remove task by name]");
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Task task = TaskService.Instance.removeByName(name, userId);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по идентификатору
     */
    public int removeTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Remove task by id]");
        System.out.println("[Please, enter task id:]");
        final long id = scanNextLong();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Task task = TaskService.Instance.removeById(id, userId);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }


    /**
     * Просмотр задачи по идентификатору
     */
    public int viewTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        System.out.println("Enter, task id: ");
        final long id = scanNextLong();
        User user = UserService.Instance.getUserBySession(sessionId);
        final Task task = TaskService.Instance.findById(id, user.getId());
        if (checkAdminGrants(user) || user.getId().equals(task.getUserId())) {
            viewTask(task);
            return 0;
        } else System.out.println("You don't have enough privileges!");
        return 0;
    }

    /**
     * Просмотр списка задач
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View task]");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[Ok]");
    }



    public int listTask(final UUID sessionId) {
        User user = UserService.Instance.getUserBySession(sessionId);
        logger.info("[List task]");
        if (checkAdminGrants(user)) viewTasks(TaskService.Instance.findAll());
        else viewTasks(TaskService.Instance.findAll(user.getId()));
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Вывод списка задач
     */
    private void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        Collections.sort(tasks, Comparator.comparing(Task::getName));
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + "description:" + task.getDescription() + " user: " + UserService.Instance.getUserById(task.getUserId()).getLogin());
            index++;
        }
    }

    /**
     * Вывод списка задач в проекте по id
     */
    public int listTaskByProjectId(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[List task by project]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        User user = UserService.Instance.getUserBySession(sessionId);
        final List<Task> tasks = TaskService.Instance.findAllByProjectId(projectId, user.getId());
        viewTasks(tasks);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Добавление задачи в проект по id
     */
    public int addTaskToProjectByIds(final UUID sessionId) throws TaskNotFoundException, ProjectNotFoundException {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Add task to project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        ProjectTaskService.Instance.addTaskToProject(projectId, taskId, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из проект по id
     */
    public int removeTaskToProjectByIds(final UUID sessionId) {
        if (!UserService.Instance.checkSession(sessionId)) return 1;
        logger.info("[Remove task from project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        ProjectTaskService.Instance.removeTaskFromProject(projectId, taskId, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Назначение задачи пользователю
     */
    public int addTaskToUserId() throws TaskNotFoundException {
        logger.info("[Add task to user]");
        System.out.println("Enter, task id: ");
        final Long taskId = scanNextLong();
        System.out.println("Enter, user id: ");
        final Long userId = scanNextLong();
        if (userId == null || taskId == null) return 1;
        Task task;
        task = TaskService.Instance.findById(taskId);
        task.setUserId(userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * экспорт в json
     */
    public void exportToJSON() throws IOException {
        logger.info("[Export tasks data to JSON]");
        TaskRepository.Instance.exportToJSON();
        System.out.println("[Ok]");
    }

    /**
     * импорт из json
     */
    public void importByJSON() throws IOException {
        logger.info("[Import tasks data by JSON]");
        TaskRepository.Instance.importByJSON();
        System.out.println("[Ok]");
    }

    /**
     * экспорт в xml
     */
    public void exportToXML() throws IOException {
        logger.info("[Export tasks data to XML]");
        TaskRepository.Instance.exportToXML();
        System.out.println("[Ok]");
    }

    /**
     * импорт из xml
     */
    public void importByXML() throws IOException {
        logger.info("[Import tasks data by XML]");
        TaskRepository.Instance.importByXML();
        System.out.println("[Ok]");
    }



}
