package ru.goryainov.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.repository.UserRepository;
import ru.goryainov.tm.util.HashMD5;


import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class UserService extends AbstractService{

    public static final UserService Instance = new UserService();
    private final Logger logger = LogManager.getLogger(UserService.class);
    private final HashMap userSession = new HashMap<UUID, User>();

    private UserService() {
    }

    public User create(final String firstName, final String lastName, final String middleName,
                       final String login, final String password, final String roleString) {
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        return UserRepository.Instance.create(firstName,lastName, middleName, login, password, role);
    }


    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return UserRepository.Instance.create(login, password);
    }

    public void clear() {
        UserRepository.Instance.clear();
    }

    public List<User> findAll() {
        return UserRepository.Instance.findAll();
    }

    public User findByIndex(final int index) {
        if (index < 0 || index > UserRepository.Instance.size() - 1) return null;
        return UserRepository.Instance.findByIndex(index);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return UserRepository.Instance.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null) return null;
        return UserRepository.Instance.findByLogin(login);
    }

    public User removeByIndex(final int index) {
        if (index < 0 || index > UserRepository.Instance.size() - 1) return null;
        return UserRepository.Instance.removeByIndex(index);
    }

    public User removeById(final Long id) {
        if (id == null) return null;
        return UserRepository.Instance.removeById(id);
    }

    public User removeByName(final String firstName, final String lastName, final String middleName) {
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        return UserRepository.Instance.removeByName(firstName, lastName, middleName);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return UserRepository.Instance.removeByLogin(login);
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName, final String login, final String roleName) {
        if (id == null) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (roleName == null || roleName.isEmpty()) return null;
        Role role = getRoleFromString(roleName);
        if (role == null) return null;
        return UserRepository.Instance.updateById(id, firstName, lastName, middleName, login, role);
    }

    private Role getRoleFromString(final String roleString) {
            return Role.valueOf(roleString);
    }

    public User authentification(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return UserRepository.Instance.authentification(login, HashMD5.md5(password));
    }

    /**
     * Cоздание пользователя
     */
    public int createUser() {
        logger.info("[Create user]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("Please, enter user role:");
        String role = scanNextLine();
        System.out.println("Please, enter user password:");
        String pwd = scanNextLine();
        UserService.Instance.create(firstName, lastName, middleName, login, pwd, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по индексу
     */
    public int updateUserByIndex() {
        logger.info("[Update user by index]");
        System.out.println("[Please, enter user index:]");
        final int index = Integer.parseInt(scanNextLine()) - 1;
        final User user = UserService.Instance.findByIndex(index);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        UserService.Instance.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по идентификатору
     */
    public int updateUserById() {
        logger.info("[Update user by id]");
        System.out.println("[Please, enter user id:]");
        final long id = Long.parseLong(scanNextLine());
        final User user = UserService.Instance.findById(id);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        UserService.Instance.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по логину
     */
    public int updateUserByLogin() {
        logger.info("[Update user by login]");
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        final User user = UserService.Instance.findByLogin(login);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        UserService.Instance.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по имени
     */
    public int removeUserByName() {
        logger.info("[Remove user by name]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        final User user = UserService.Instance.removeByName(firstName, lastName, middleName);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по идентификатору
     */
    public int removeUserById() {
        logger.info("[Remove user by id]");
        System.out.println("[Please, enter user id:]");
        final long id = scanNextLong();
        final User user = UserService.Instance.removeById(id);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по логину
     */
    public int removeUserByLogin() {
        logger.info("[Remove user by login]");
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        final User user = UserService.Instance.removeByLogin(login);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по индексу
     */
    public int removeUserByIndex() {
        logger.info("[Remove user by index]");
        System.out.println("[Please, enter user index:]");
        final int index = scanNextInt() - 1;
        final User user = UserService.Instance.removeByIndex(index);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистки проекта
     */
    public int clearUser() {
        logger.info("[Clear user]");
        UserService.Instance.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр списка пользователей
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[View user]");
        System.out.println("Id: " + user.getId());
        System.out.println("FirstName: " + user.getFirstName());
        System.out.println("LasttName: " + user.getLastName());
        System.out.println("MiddleName: " + user.getMiddleName());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("[Ok]");
    }

    /**
     * Просмотр пользователя по индексу
     */
    public int viewUserByIndex() {
        System.out.println("Enter, user index: ");
        final int index = scanNextInt() - 1;
        final User user = UserService.Instance.findByIndex(index);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по идентификатору
     */
    public int viewUserById() {
        System.out.println("Enter, user id: ");
        final Long id = scanNextLong();
        final User user = UserService.Instance.findById(id);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по логину
     */
    public int viewUserByLogin() {
        System.out.println("Enter, user login: ");
        final String login = scanNextLine();
        final User user = UserService.Instance.findByLogin(login);
        viewUser(user);
        return 0;
    }

    /**
     * Вывод списка пользователей
     */
    public int listUser() {
        logger.info("[List user]");
        int index = 1;
        for (final User user : UserService.Instance.findAll()) {
            System.out.println(Integer.toString(index) + '.');
            viewUser(user);
            index++;
        }
        /*System.out.println("[Ok]");*/
        return 0;
    }

    /**
     * Логон пользователя
     */
    public UUID authentification() {
        logger.info("Logon");
        System.out.println("Please, enter login...");
        String login = scanNextLine();
        System.out.println("Please, enter password...");
        String password = scanNextLine();
        User user = UserService.Instance.authentification(login, password);
        if (user != null) {
            UUID sessionId = UUID.randomUUID();
            userSession.put(sessionId, user);
            System.out.println("Authentication succeeded!");
            System.out.println("session id:" + sessionId);
            return sessionId;
        }
        System.out.println("Authentication failed!");
        return null;
    }


    /**
     * Логоф пользователя
     */
    public void logOff() {
        logger.info("Log Off");
        System.out.println("Please, enter session id...");
        UUID sessionId = UUID.fromString(scanNextLine());
        if (sessionId != null) {
            if (userSession.remove(sessionId) != null) {
                System.out.println("Log Off session " + sessionId.toString() + "succeeded!");
                System.out.println();
                return;
            }
            System.out.println("Log Off failed! Unknown session:" + sessionId.toString());
            return;
        }
        System.out.println("Log Off failed! ");
    }

    /**
     * Проверка аторизации пользователя
     */
    public boolean checkSession(UUID sessionId) {
        if (userSession.containsKey(sessionId)) return true;
        System.out.println("Access denied! Please, log in...");
        return false;
    }

    /**
     * Поиск пользователя по сессии
     */
    public User getUserBySession(UUID sessionId) {
        User user = (User) userSession.getOrDefault(sessionId, null);
        if (user != null) return user;
        System.out.println("Access denied! Please, log in...");
        return null;
    }

    /**
     * Поиск пользователя по id
     */
    public User getUserById(Long userId) {
        User user = UserService.Instance.findById(userId);
        if (user != null) return user;
        return null;
    }

    /**
     * Поиск пользователя по login
     */
    public User getUserByLogin(String userLogin) {
        User user = UserService.Instance.findByLogin(userLogin);
        if (user != null) return user;
        return null;
    }

    /**
     * Смена пароля пользователя
     */
    public void changePasswordByUserLogin(final UUID sessionId) {
        if (!checkSession(sessionId)) return;
        User userByLogin = getUserBySession(sessionId);
        if (userByLogin == null) return;
        logger.info("Change user password");
        System.out.println("Please, enter user login...");
        String login = scanNextLine();
        if (login == null || login.isEmpty()) return;
        User userChange = getUserByLogin(login);
        if (userByLogin.getId() == userChange.getId() || checkAdminGrants(userByLogin)) {
            System.out.println("Please, enter new password...");
            String passwordChange = scanNextLine();
            userChange.setPasswordHash(passwordChange);
            System.out.println("Ok");
            return;
        }
        System.out.println("You don't have enough privileges!");
    }


}
