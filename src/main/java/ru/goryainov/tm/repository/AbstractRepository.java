package ru.goryainov.tm.repository;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ru.goryainov.tm.entity.Project;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T> {
    protected final List<T> itemsList = new ArrayList<>();

    public void create(T item) {
        if (!itemsList.contains(item))
            itemsList.add(item);
    }

    public void remove(T item) {
        itemsList.remove(item);
    }

    public List<T> findAll() {
        return itemsList;
    }

    public abstract T findById(final Long id);

    public abstract T findByName(final String name, final Long userId);

    public abstract T removeById(final Long id, final Long userId);

    public abstract T removeByName(final String name, final Long userId);

    public int size() {
        return itemsList.size();
    }

    private static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }


    public void exportToJSON(String file_name) throws IOException {
        Gson gson = new Gson();
        OutputStreamWriter objectOutputStream = new OutputStreamWriter(new FileOutputStream(file_name.concat(".json")));
        objectOutputStream.write(gson.toJson(itemsList));
        objectOutputStream.close();
    }

    public void exportToXML(String file_name) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File(file_name.concat(".xml")), itemsList);
    }

    public void importByJSON(String file_name, Class<T> clazz) throws IOException {
        Gson gson = new Gson();
        InputStreamReader objectInputStream = null;
        try {
            objectInputStream = new InputStreamReader(new FileInputStream(file_name.concat(".json")));
            String result = new BufferedReader(objectInputStream).lines()
                    .parallel().collect(Collectors.joining("\n"));
            Type typeOfT = TypeToken.getParameterized(List.class, clazz).getType();
            itemsList.clear();
            itemsList.addAll(gson.fromJson(result, typeOfT));
        } finally {
            if (objectInputStream != null)
                objectInputStream.close();
        }
    }

    public void importByXML(String file_name, Class<T[]> clazz) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(new FileInputStream(new File(file_name.concat(".xml"))));
        T[] arr = xmlMapper.readValue(xml, clazz);
        itemsList.clear();
        for (T item: arr) {
            itemsList.add(item);
        }
    }

}
