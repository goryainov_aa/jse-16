package ru.goryainov.tm.repository;


import com.google.gson.Gson;
import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.User;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> {

    private HashMap<String, List<Project>> project_map;
    public static final ProjectRepository Instance = new ProjectRepository();

    private ProjectRepository() {
        project_map = new HashMap();
    }

    private void save(Project project, String name) {
        List<Project> lst;
        create(project);
        lst = project_map.get(name);
        if (lst == null) {
            lst = new ArrayList<>();
            project_map.put(name, lst);
        }
        lst.add(project);
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name);
        project.setDescription(description);
        save(project, name);
        return project;
    }

    public Project create(final String name, final String description, final User user) {
        final Project project = new Project(name);
        project.setDescription(description);
        project.setUserId(user.getId());
        save(project, name);
        return project;
    }

    public Project update(final Long id, final String description, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    public void clear(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        for (Project project : userProjects) {
            remove(project);
            project_map.get(project.getName()).remove(project);
        }
    }

    public Project findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty() || userId == null) return null;
        //List<Project> result = new ArrayList<>();
        for (final Project project : project_map.get(name)) {
            if (project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    public Project findById(final Long id, final Long userId) {
        if (id == null || userId == null) return null;
        for (final Project project : itemsList) {
            if (project.getUserId() == null || !project.getUserId().equals(userId)) continue;
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : itemsList) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public List<Project> findByUserId(final Long userId) {
        List<Project> userProjects = new ArrayList<>();
        for (Project project : itemsList) {
            if (project.getUserId() == null || !project.getUserId().equals(userId)) continue;
            userProjects.add(project);
        }
        return userProjects;
    }

    public Project removeById(final Long id, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        remove(project);
        return project;
    }

    public Project removeByName(final String name, final Long userId) {
        final Project projects = findByName(name, userId);
        if (projects == null) return null;
        remove(projects);
        project_map.remove(projects);
        return projects;
    }


    public void exportToJSON() throws IOException {
        exportToJSON("projects");
    }

    public void importByJSON() throws IOException {
        importByJSON("projects", Project.class);
    }

    public void  exportToXML() throws IOException {
        exportToXML("projects");
    }

    public void importByXML() throws IOException {
        importByXML("projects", Project[].class);
    }

    public List<Project> findAll(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        return userProjects;
    }

    public List<Project> findAll() {
        List<Project> userProjects = new ArrayList<>();
        for (Project project : itemsList) {
            userProjects.add(project);
        }
        return userProjects;
    }
}
