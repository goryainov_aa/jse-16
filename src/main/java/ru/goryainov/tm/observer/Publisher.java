package ru.goryainov.tm.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.service.SystemService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ru.goryainov.tm.constant.TerminalConst.EXIT;


public class Publisher {

    private static final Logger logger = LogManager.getLogger(Publisher.class);
    private static Publisher instance;
    protected final Scanner scanner = new Scanner(System.in);
    private List<Subscriber> subscribers;

    public Publisher() {
        subscribers = new ArrayList<>();
    }

    public void addSubscriber(Subscriber subscriber){
        subscribers.add(subscriber);
    }

    public void delSubscriber(Subscriber subscriber){
        subscribers.remove(subscriber);
    }

    public void run(){
        SystemService.Instance.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                for (Subscriber subscriber : subscribers) {
                    subscriber.handle(command);
                }
                SystemService.Instance.commandLogging(command);
            } catch (ProjectNotFoundException | TaskNotFoundException exception) {
                /*app.systemController.displayError(exception);*/
                logger.error(exception);
            }
        }
    }

}
