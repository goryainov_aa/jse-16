package ru.goryainov.tm.observer;

import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.service.ProjectService;

import java.util.UUID;

public abstract class Subscriber {

    protected static UUID sessionId;

    public abstract void handle(String param) throws TaskNotFoundException, ProjectNotFoundException;

}
