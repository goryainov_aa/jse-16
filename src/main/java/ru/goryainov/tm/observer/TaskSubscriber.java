package ru.goryainov.tm.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.service.ProjectService;
import ru.goryainov.tm.service.TaskService;

import java.io.IOException;

import static ru.goryainov.tm.constant.TerminalConst.*;

public class TaskSubscriber extends Subscriber {

    private static final Logger logger = LogManager.getLogger(TaskSubscriber.class);

    @Override
    public void handle(String param) {
        /*if (param == null || param.isEmpty()) {
            logger.error("Command is empty!");
            return;
        }*/
        switch (param) {
            case TASK_CREATE:
                TaskService.Instance.createTask(sessionId);
                break;
            case TASK_CLEAR:
                TaskService.Instance.clearTask(sessionId);
                break;
            case TASK_LIST:
                TaskService.Instance.listTask(sessionId);
                break;
            case TASK_VIEW_BY_ID:
                try {
                    TaskService.Instance.viewTaskById(sessionId);

                } catch (TaskNotFoundException e) {
                    logger.error("Task by id not found." + e.getMessage());
                }
                break;
            case TASK_REMOVE_BY_NAME:
                TaskService.Instance.removeTaskByName(sessionId);
                break;
            case TASK_REMOVE_BY_ID:
                try {
                    TaskService.Instance.removeTaskById(sessionId);
                } catch (TaskNotFoundException e) {
                    logger.error("Task by id not deleted." + e.getMessage());
                }
                break;
            case TASK_UPDATE_BY_ID:
                try {
                    TaskService.Instance.updateTaskById(sessionId);
                } catch (TaskNotFoundException e) {
                    logger.error("Task by id not updated." + e.getMessage());
                }
                break;
            case TASK_ADD_TO_PROJECT_BY_IDS:
                try {
                    TaskService.Instance.addTaskToProjectByIds(sessionId);
                } catch (TaskNotFoundException e) {
                    logger.error("Task by id not added to project. Task not found" + e.getMessage());
                } catch (ProjectNotFoundException projectNotFoundException) {
                    logger.error("Task by id not added to project. Project not found" + projectNotFoundException.getMessage());
                }
                break;
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                TaskService.Instance.removeTaskToProjectByIds(sessionId);
                break;
            case TASK_LIST_BY_PROJECT_ID:
                TaskService.Instance.listTaskByProjectId(sessionId);
                break;
            case TASK_ADD_USER_BY_ID:
                try {
                    TaskService.Instance.addTaskToUserId();
                } catch (TaskNotFoundException e) {
                    logger.error("Task by id not added to user." + e.getMessage());
                }
                break;
            case TASK_EXPORT_TO_JSON:
                try {
                    TaskService.Instance.exportToJSON();
                } catch (IOException e) {
                    logger.error("Task not export to JSON." + e.getMessage());
                }
                break;
            case TASK_EXPORT_TO_XML:
                try {
                    TaskService.Instance.exportToXML();
                } catch (IOException e) {
                    logger.error("Task not export to XML." + e.getMessage());
                }
                break;
            case TASK_IMPORT_BY_JSON:
                try {
                    TaskService.Instance.importByJSON();
                } catch (IOException e) {
                    logger.error("Task not import by JSON." + e.getMessage());
                }
                break;
            case TASK_IMPORT_BY_XML:
                try {
                    TaskService.Instance.importByXML();
                } catch (IOException e) {
                    logger.error("Task not import by XML." + e.getMessage());
                }
                break;
        }
    }
}
