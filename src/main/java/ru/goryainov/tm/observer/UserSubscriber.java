package ru.goryainov.tm.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.service.SystemService;
import ru.goryainov.tm.service.UserService;

import java.util.UUID;

import static ru.goryainov.tm.constant.TerminalConst.*;
import static ru.goryainov.tm.constant.TerminalConst.USER_CHANGE_PASSWORD;

public class UserSubscriber extends Subscriber{

    private static final Logger logger = LogManager.getLogger(UserSubscriber.class);

    @Override
    public void handle(String param) {
        /*if (param == null || param.isEmpty()) {
            logger.error("Command is empty!");
            return;
        }*/
        switch (param) {
            case USER_CREATE:
                 UserService.Instance.createUser();
                break;
            case USER_LIST:
                 UserService.Instance.listUser();
                break;
            case USER_CLEAR:
                 UserService.Instance.clearUser();
                break;
            case USER_UPDATE_BY_INDEX:
                 UserService.Instance.updateUserByIndex();
                break;
            case USER_UPDATE_BY_ID:
                 UserService.Instance.updateUserById();
                break;
            case USER_UPDATE_BY_LOGIN:
                 UserService.Instance.updateUserByLogin();
                break;
            case USER_REMOVE_BY_NAME:
                 UserService.Instance.removeUserByName();
                break;
            case USER_REMOVE_BY_INDEX:
                 UserService.Instance.removeUserByIndex();
                break;
            case USER_REMOVE_BY_ID:
                 UserService.Instance.removeUserById();
                break;
            case USER_REMOVE_BY_LOGIN:
                 UserService.Instance.removeUserByLogin();
                break;
            case USER_VIEW_BY_ID:
                 UserService.Instance.viewUserById();
                break;
            case USER_VIEW_BY_INDEX:
                 UserService.Instance.viewUserByIndex();
                break;
            case USER_VIEW_BY_LOGIN:
                 UserService.Instance.viewUserByLogin();
                break;
            case USER_AUTHENTIFICATION:
                sessionId = UserService.Instance.authentification();
                if (sessionId != null) SystemService.Instance.newSession();
                break;
            case USER_LOGOFF:
                UserService.Instance.logOff();
                break;
            case USER_CHANGE_PASSWORD:
                UserService.Instance.changePasswordByUserLogin(sessionId);
                break;
        }
    }
}
