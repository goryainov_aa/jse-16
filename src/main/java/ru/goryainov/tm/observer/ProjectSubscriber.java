package ru.goryainov.tm.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.service.ProjectService;


import java.io.IOException;

import static ru.goryainov.tm.constant.TerminalConst.*;

public class ProjectSubscriber extends Subscriber {
    private static final Logger logger = LogManager.getLogger(ProjectSubscriber.class);


    @Override
    public void handle(String param) {
        /*if (param == null || param.isEmpty()) {
            logger.error("Command is empty!");
            return;
        }*/
        switch (param) {
            case PROJECT_CREATE:
                ProjectService.Instance.createProject(sessionId);
                break;
            case PROJECT_CLEAR:
                ProjectService.Instance.clearProject(sessionId);
                break;
            case PROJECT_LIST:
                ProjectService.Instance.listProject(sessionId);
                break;
            case PROJECT_VIEW_BY_ID:
                try {
                    ProjectService.Instance.viewProjectById(sessionId);
                } catch (ProjectNotFoundException e) {
                    logger.error("Project not found  by id." + e.getMessage());
                }
                break;
            case PROJECT_REMOVE_BY_NAME:
                ProjectService.Instance.removeProjectByName(sessionId);
                break;
            case PROJECT_REMOVE_BY_ID:
                try {
                    ProjectService.Instance.removeProjectById(sessionId);
                } catch (ProjectNotFoundException e) {
                    logger.error("Project not deleted  by id." + e.getMessage());
                }
                break;
            case PROJECT_UPDATE_BY_ID:
                try {
                    ProjectService.Instance.updateProjectById(sessionId);
                } catch (ProjectNotFoundException e) {
                    logger.error("Project not updated  by id." + e.getMessage());
                }
                break;
            case PROJECT_ADD_USER_BY_ID:
                try {
                    ProjectService.Instance.addProjectToUserId();
                } catch (ProjectNotFoundException e) {
                    logger.error("Project not added  by userId." + e.getMessage());
                }
                break;
            case PROJECT_EXPORT_TO_JSON:
                try {
                    ProjectService.Instance.exportToJSON();
                } catch (IOException e) {
                    logger.error("Project not export to JSON." + e.getMessage());
                }
                break;
            case PROJECT_EXPORT_TO_XML:
                try {
                    ProjectService.Instance.exportToXML();
                } catch (IOException e) {
                    logger.error("Project not export to XML." + e.getMessage());
                }
                break;
            case PROJECT_IMPORT_BY_JSON:
                try {
                    ProjectService.Instance.importByJSON();
                } catch (IOException e) {
                    logger.error("Project not export to JSON." + e.getMessage());
                }
                break;
            case PROJECT_IMPORT_BY_XML:
                try {
                    ProjectService.Instance.importByXML();
                } catch (IOException e) {
                    logger.error("Project not export to XML." + e.getMessage());
                }
                break;
        }

    }
}
