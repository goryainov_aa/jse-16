package ru.goryainov.tm.observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.goryainov.tm.service.SystemService;
import static ru.goryainov.tm.constant.TerminalConst.*;


public class SystemSubscriber extends Subscriber {
    private static final Logger logger = LogManager.getLogger(SystemSubscriber.class);


    @Override
    public void handle(String param) {
        /*if (param == null || param.isEmpty()) {
            logger.error("Command is empty!");
            return;
        }*/
        switch (param) {
            case VERSION:
                SystemService.Instance.displayVersion();
                break;
            case ABOUT:
                SystemService.Instance.displayAbout();
                break;
            case HELP:
                SystemService.Instance.displayHelp();
                break;
            case EXIT:
                SystemService.Instance.displayExit();
                break;
            case VIEW_COMMANDS_LOG:
                SystemService.Instance.displayCommandsLog();
                break;
        }
    }
}
