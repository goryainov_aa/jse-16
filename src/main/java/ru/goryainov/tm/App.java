package ru.goryainov.tm;

import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.observer.*;
import ru.goryainov.tm.repository.UserRepository;


/**
 * Тестовое приложение с поддержкой аргументов запуска
 */

public class App {

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */
    public static void main(final String[] args) {

        UserRepository.Instance.create("", "", "", "admin", "qwerty", Role.ADMIN);
        UserRepository.Instance.create("", "", "", "user", "qwerty", Role.USER);
        Publisher publisher = new Publisher();
        publisher.addSubscriber(new ProjectSubscriber());
        publisher.addSubscriber(new UserSubscriber());
        publisher.addSubscriber(new TaskSubscriber());
        publisher.addSubscriber(new SystemSubscriber());
        publisher.run();
    }


}


